import Homepage from "pages/Homepage";
import { Route, BrowserRouter } from "react-router-dom";

export default function Routes() {
  return (
    <BrowserRouter>
      <Route path="/" exact >
        <Homepage />
      </Route>
    </BrowserRouter>
  );
}
