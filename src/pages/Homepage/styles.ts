import styled from "styled-components";

export const ContentHeader = styled.div`
  display: flex;
  gap: 1rem;
  min-height: 100vh;
  align-items: center;
  justify-content: space-between;

  section {
    max-width: 50%;
    display: flex;
    flex-direction: column;
    gap: 1rem;

    h1 {
      font-family: Arvo, serif;
      font-weight: 700;
      font-size: 2.5rem;
      line-height: 3rem;
    }

    p {
      font-family: Khula, sans-serif;
      font-weight: 400;
      font-size: 1.5rem;
      line-height: 2rem;
    }
  }

  img {
    object-fit: contain;
    margin: 0 auto;
  }

  @media (max-width: 768px) {
    display: flex;
    align-items: flex-start;
    margin: 2.5rem 0;

    min-height: 100%;

    section {
      max-width: 100%;
      flex: 1;

      h1 {
        font-family: Arvo, serif;
        font-weight: 700;
        font-size: 4rem;
        line-height: 4.5rem;
      }

      p {
        font-family: Khula, sans-serif;
        font-weight: 400;
        font-size: 2rem;
        line-height: 2.5rem;
      }
    }
    img {
      display: none;
    }
  }

  @media (max-width: 428px) {
    display: flex;
    margin: 2rem 0;
    align-items: flex-start;
    min-height: 100%;

    section {
      max-width: 100%;

      h1 {
        font-family: Arvo, serif;
        font-weight: 700;
        font-size: 2rem;
        line-height: 2.3rem;
      }

      p {
        font-family: Khula, sans-serif;
        font-weight: 400;
        font-size: 1rem;
        line-height: 1.3rem;
      }
    }

    img {
      display: none;
    }
  }
`;

export const BlockButtons = styled.div`
  display: flex;
  gap: 1rem;
`;

export const Footer = styled.div`
  background-color: var(--dark);
  height: 3rem;
  min-width: 100vw;

  margin: 0 auto;
`;
