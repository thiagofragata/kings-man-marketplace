import React from "react";

import { ButtonDefault, Container } from "../../styles/commonStyles";

import ImgRelogio from "assets/images/relogio.png";
import { BlockButtons, ContentHeader, Footer } from "./styles";

import { Navbar } from "pages/components/NavBar/NavBar";
import { Showcase } from "pages/components/Showcase";
import { Newsletter } from "pages/components/Newsletter";

import { Link } from "react-scroll";

export default function Homepage() {
  function handleAlert() {
    alert("EM DESENVOLVIMENTO ...");
  }
  return (
    <>
      <Container>
        <Navbar />
        <ContentHeader>
          <section>
            <h1>The biggest watch marketplace in the world</h1>
            <p>
              Best price guarantee - in the unlikely event you find a lower
              King's Man price, we'll refund the difference.
            </p>
            <BlockButtons>
              <ButtonDefault>
                <Link
                  activeClass="active"
                  to="showcase"
                  spy={true}
                  smooth={true}
                  offset={-25}
                  duration={500}
                >
                  {/* eslint-disable-next-line */}
                  <a>Get start</a>
                </Link>
              </ButtonDefault>
              <ButtonDefault className="secondary" onClick={handleAlert}>
                Contact sales
              </ButtonDefault>
            </BlockButtons>
          </section>
          <img src={ImgRelogio} alt="Relogio" />
        </ContentHeader>

        <Showcase />

        <Newsletter />
      </Container>
      <Footer />
    </>
  );
}
