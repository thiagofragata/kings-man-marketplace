/* eslint-disable */
import { Link } from "react-scroll";


import { Ul } from "./styles";

export function RightNav({ open }: any) {
  return (
    <Ul open={open}>
      <li>
        <Link
          activeClass="active"
          to="header"
          spy={true}
          smooth={true}
          offset={-70}
          duration={500}
        >
          <a>Inicio</a>
        </Link>
      </li>
      <li>
        <Link
          activeClass="active"
          to="header"
          spy={true}
          smooth={true}
          offset={-70}
          duration={500}
        >
          <a>About</a>
        </Link>
      </li>
      <li>
        <Link
          activeClass="active"
          to="header"
          spy={true}
          smooth={true}
          offset={-70}
          duration={500}
        >
          <a>Contact</a>
        </Link>
      </li>
    </Ul>
  );
}
