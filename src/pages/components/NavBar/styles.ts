import styled from "styled-components";

export const Container = styled.div`
  width: 100%;
  padding: 1rem 0;
  background: transparent;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const Nav = styled.nav`
  width: 100%;
  display: flex;
  gap: 8rem;
  align-items: center;

  color: var(--dark);
  text-transform: uppercase;
`;

interface UlProps {
  open: boolean;
}

export const Ul = styled.ul<UlProps>`
  list-style: none;
  display: flex;
  gap: 2rem;

  li {
    cursor: pointer;

    a {
      color: var(--white);
      transition: border 0.2s;

      &:hover {
        border-bottom: 2px solid var(--dark);
      }
    }
  }

  @media (max-width: 768px) {
    flex-flow: column nowrap;
    background-color: var(--dark);

    position: fixed;
    transform: ${({ open }) => (open ? "translateX(0)" : "translateX(100%)")};
    top: 0;
    right: 0;

    height: 100vh;
    width: 200px;
    padding-top: 6rem;
    transition: transform 0.3s ease-in-out;

    li {
      cursor: pointer;
      padding: 0 2rem;
      font-weight: 400;

      a {
        color: var(--light);
        transition: border 0.2s;

        &:hover {
          border-bottom: 2px solid var(--dark);
        }
      }
    }
  }
`;

export const StyledBurger = styled.div<UlProps>`
  width: 3.05rem;
  height: 3.05rem;
  position: fixed;
  top: 1rem;
  right: 1.5rem;
  z-index: 120;
  display: none;

  padding: 0.5rem;

  background-size: 4rem 4rem;
  border-radius: 1rem;

  @media (max-width: 768px) {
    display: flex;
    justify-content: space-around;
    flex-flow: column nowrap;
  }

  div {
    width: 2rem;
    height: 0.25rem;

    background-color: ${({ open }) => (open ? "#E73F5D" : "#121212")};
    border-radius: 0.5rem;
    transform-origin: 1px;
    transition: all 0.3s linear;

    &:nth-child(1) {
      transform: ${({ open }) => (open ? "rotate(45deg)" : "rotate(0)")};
    }
    &:nth-child(2) {
      transform: ${({ open }) => (open ? "translateX(100%)" : "translateX(0)")};
      opacity: ${({ open }) => (open ? 0 : 1)};
    }
    &:nth-child(3) {
      transform: ${({ open }) => (open ? "rotate(-45deg)" : "rotate(0)")};
    }
  }
`;
