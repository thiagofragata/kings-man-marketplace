// eslint-disable
import { Container, Nav } from "./styles";
import { Burger } from "./Burger";

import logo from "assets/images/logo.svg";

export function Navbar() {
  return (
    <Container>
      <Nav id="navbar">
        <img src={logo} alt="King's Man" />
        <Burger />
      </Nav>
    </Container>
  );
}
