import React from "react";

import { ButtonDefault } from "styles/commonStyles";
import { ContainerCard } from "./styled";

interface CardProps {
  price: string;
  name: string;
  reference: string;
  image: string;
}

export function Card(props: CardProps) {
  return (
    <ContainerCard>
      <img src={props.image} alt={props.name} />
      <div>
        <h2>{props.name}</h2>
        <h1>
          {new Intl.NumberFormat("en-Us", {
            currency: "USD",
            style: "currency",
          }).format(parseFloat(props.price) / 100)}
        </h1>
        <p>{props.reference}</p>
        <ButtonDefault>Buy</ButtonDefault>
      </div>
    </ContainerCard>
  );
}
