import styled from "styled-components";

export const ContainerCard = styled.div`
  border: 2px solid var(--dark);
  border-radius: 0.5rem;

  padding: 1rem;

  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;

  img {
    object-fit: contain;

    max-height: 190px;
    max-width: 190px;

    transition: transform 0.2s;

    &:hover {
      transform: scale(1.15);
    }
  }

  > div {
    width: 100%;

    display: flex;
    flex-direction: column;
    justify-content: center;

    h2 {
      font-family: Arvo, serif;
      font-weight: 400;
      font-size: 1rem;
      line-height: 1.2rem;

      color: var(--gray);
      text-align: left;
    }

    h1 {
      font-family: Khula, sans-serif;
      font-weight: 600;
      font-size: 1.5rem;
      line-height: 2rem;

      color: var(--dark);
      text-align: left;

      margin: 0;
    }

    p {
      font-family: Khula, sans-serif;
      font-weight: 400;
      font-size: 0.8rem;
      line-height: 1rem;

      color: var(--gray);
      text-align: left;
    }
  }

  @media (max-width: 428px) {
    border: 2px solid var(--dark);
    border-radius: 0.5rem;

    padding: 0.5rem;

    display: flex;
    flex-direction: column;
    justify-content: space-between;

    img {
      object-fit: contain;
      padding: 0.5rem;

      max-height: 120px;
      max-width: 120px;

      transition: transform 0.2s;

      &:hover {
        transform: scale(1.15);
      }
    }
  }
`;
