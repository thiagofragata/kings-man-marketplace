import styled from "styled-components";

export const Container = styled.div`
  h1 {
    font-family: Arvo, serif;
    font-weight: 700;
    font-size: 2rem;
    line-height: 3rem;

    color: var(--dark);
    text-align: center;
    margin-bottom: 3rem;
  }
`;

export const Cards = styled.section`
  display: grid;
  grid-template-columns: repeat(4, 1fr);

  gap: 1rem;

  @media (max-width: 768px) {
    display: grid;
    grid-template-columns: repeat(3, 1fr);

    gap: 1rem;
    padding: 1rem;
  }

  @media (max-width: 468px) {
    display: grid;
    grid-template-columns: repeat(2, 1fr);

    gap: 1rem;
  }

  @media (max-width: 320px) {
    display: grid;
    grid-template-columns: repeat(1, 1fr);

    gap: 1rem;
  }
`;

/* @media (max-width: 768px) {
  }

  @media (max-width: 428px) {
  } */
