import React from "react";
import { css } from "@emotion/react";
import ClipLoader from "react-spinners/ClipLoader";

import { useEffect, useState } from "react";

import api from "../../../services/api";

import { Card } from "pages/components/Card";
import { Cards, Container } from "./styles";

const override = css`
  display: block;
  margin: 0 auto;
`;

interface productsProps {
  id: string;
  price: string;
  product_name: string;
  product_reference: string;
  product_title: string;
  image_url: string;
}

export function Showcase() {
  const [products, setProducts] = useState<productsProps[]>([]);

  const [loading, setLoading] = useState(true);

  // eslint-disable-next-line
  const [color, setColor] = useState("#121212");

  useEffect(() => {
    api
      .get("/products")
      .then((response) => {
        const results = response.data;
        setProducts(results);
        setLoading(false);
      })
      .catch((err) => {
        console.error("Ops! " + err);
      });
  }, []);

  return (
    <Container id="showcase">
      <h1>See our offers</h1>
      {loading ? (
        <>
          <ClipLoader
            color={color}
            loading={loading}
            css={override}
            size={100}
          />
        </>
      ) : (
        <>
          <Cards>
            {products.map((product) => (
              <Card
                key={product.id}
                name={product.product_name}
                reference={product.product_reference}
                image={product.image_url}
                price={product.price}
              />
            ))}
          </Cards>
        </>
      )}
    </Container>
  );
}
