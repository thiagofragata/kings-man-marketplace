import React from "react";

import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";

import toast, { Toaster } from "react-hot-toast";

import { ButtonSubmit } from "styles/commonStyles";
import { Container, FormNewsletter } from "./styles";

interface IFormInputs {
  FirstName: string;
  Email: string;
}

const schema = yup
  .object({
    FirstName: yup.string().required(),
    Email: yup.string().email(),
  })
  .required();

export function Newsletter() {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<IFormInputs>({
    resolver: yupResolver(schema),
  });

  const onSubmit = (data: IFormInputs) => {
    console.log(data);

    toast("Congratulations you have been registered in our newsletter!", {
      icon: "👏",
    });
  };

  return (
    <Container>
      <div>
        <h1>Assine nossa newsletter</h1>
        <p>
          Fique por dentro das nossas novidades e receba 10% de desconto na
          primeira compra!
        </p>
      </div>

      <FormNewsletter onSubmit={handleSubmit(onSubmit)}>
        <label htmlFor="name">
          <input {...register("FirstName")} placeholder="Fist name" />
          <p>{errors.FirstName?.message}</p>
        </label>

        <label htmlFor="email">
          <input {...register("Email")} placeholder="E-mail" />
          <p>{errors.Email?.message}</p>
        </label>
        <ButtonSubmit type="submit">Enviar</ButtonSubmit>
      </FormNewsletter>
      <Toaster />
    </Container>
  );
}
