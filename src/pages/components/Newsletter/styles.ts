import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  flex-direction: column;

  align-items: center;
  justify-content: center;

  text-align: center;

  margin: 3rem auto;
  width: 100%;

  > div {
    margin-bottom: 2rem;
    h1 {
      font-family: Arvo, serif;
      font-weight: 700;
      font-size: 2rem;
      line-height: 3rem;

      color: var(--dark);
      text-align: center;
    }

    p {
      font-family: Khula, sans-serif;
      font-weight: 500;
      font-size: 1rem;
      line-height: 1.5rem;

      color: var(--gray);
      text-align: center;
    }
  }
`;

export const FormNewsletter = styled.form`
  display: flex;
  gap: 1rem;

  input {
    border: 1px solid var(--dark);
    border-radius: 8px;
    height: 48px;
    max-width: 300px;
    width: 100%;

    padding: 0 1rem;

    &::placeholder {
      text-transform: uppercase;
    }
  }

  @media (max-width: 428px) {
    display: flex;
    flex-direction: column;
    width: 100%;
    gap: 1rem;

    input {
      border: 1px solid var(--dark);
      border-radius: 8px;
      height: 48px;
      padding: 0 1rem;
      min-width: 100%;

      &::placeholder {
        text-transform: uppercase;
      }
    }
  }
`;
