import React from "react";
import ReactDOM from "react-dom";
import Routes from "routes";

import { ThemeProvider } from "styled-components";
import colors from "./styles/themes"

import { GlobalStyles } from "./styles/globals";

ReactDOM.render(
  <React.StrictMode>
    <ThemeProvider theme={colors}>
      <GlobalStyles />
      <Routes />
    </ThemeProvider>
  </React.StrictMode>,
  document.getElementById("root")
);
