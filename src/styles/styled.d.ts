import "styled-components";

declare module "styled-components" {
  interface DefaultTheme {
    colors: {
      dark: string;
      gray: string;
      light: string;
    };
  }
}
