import styled from "styled-components";
import ImgBackground from "assets/images/background.svg";

export const Container = styled.div`
  max-width: 1180px;
  margin: 0 auto;
  padding: 1rem;

  background: url(${ImgBackground}) no-repeat 80% top fixed;
  background-size: contain;

  @media (max-width: 768px) {
    padding: 1rem;
  }
  @media (max-width: 428px) {
    padding: 1rem;
    background-size: 60%;
  }
`;

export const ButtonDefault = styled.button`
  display: flex;
  align-items: center;
  justify-content: center;

  border: 0;
  border-radius: 0.5rem;
  background-color: var(--dark);
  color: var(--light);

  padding: 1rem 2rem;
  margin: 2rem 0 0;
  width: 100%;

  font-size: 1rem;
  font-weight: 500;
  text-transform: uppercase;

  &.secondary {
    border: 2px solid var(--dark);
    background-color: transparent;
    color: var(--dark);
  }

  transition: filter 0.2s;

  &:hover {
    filter: brightness(0.8);
  }

  @media (max-width: 768px) {
    padding: 0.8rem 2rem;
  }

  @media (max-width: 428px) {
    padding: 0.5rem 1.5rem;
    margin: 1rem 0 0;
  }

  @media (max-width: 320px) {
    padding: .5rem .5rem;
    margin: 1rem 0 0;

    font-size: .8rem;
    font-weight: 500;
    text-transform: uppercase;
  }
`;

export const ButtonSubmit = styled.button`
  display: flex;
  align-items: center;
  justify-content: center;

  border: 0;
  border-radius: 8px;
  background-color: var(--dark);
  color: var(--light);

  height: 48px;
  padding: 0 1rem;

  font-size: 1rem;
  font-weight: 500;
  text-transform: uppercase;

  &.secondary {
    border: 2px solid var(--dark);
    background-color: transparent;
    color: var(--dark);
  }

  transition: filter 0.2s;

  &:hover {
    filter: brightness(0.8);
  }

  @media (max-width: 768px) {
    padding: 0.8rem 2rem;
  }



  @media (max-width: 320px) {
    font-size: .8rem;
    font-weight: 500;
    text-transform: uppercase;
  }
`;


