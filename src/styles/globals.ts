import { createGlobalStyle } from "styled-components";

export const GlobalStyles = createGlobalStyle`
   :root{
    --dark: ${({ theme }) => theme.colors.dark};
    --gray: ${({ theme }) => theme.colors.gray};
    --light: ${({ theme }) => theme.colors.light};
    }
    
    ::-webkit-scrollbar {
        width: .5rem;
    }
    ::-webkit-scrollbar-track {
        background: var(--dark);
    }
    ::-webkit-scrollbar-thumb {
        background: var(--gray);
        border-radius: .5rem;
    }

    * {
        margin: 0;
        padding: 0;
        box-sizing: border-box;
        outline: none;    
    }

    html {
        @media(max-width: 1024px) {
            font-size: 93.75%; 
        }
        @media(max-width: 720px) {
            font-size: 87.5%;
        }
    }

    body {
        -webkit-font-smoothing: antialiased;
        overflow-x: hidden;
    }

    body, input, textarea, button {
        font-family: "Khula", sans-serif;
        font-weight: 700;
    }
    ul, li {
        list-style: none;
    }
    
    a {
        color: inherit;
        text-decoration: none;
    }
    h1, h2, h3, h4, h5, h6, strong {
        font-weight: 400;
        color: var(--dark);
    }
    p {
      color: var(--gray);
    }
    
    button {
        cursor: pointer;
    }
    
    [disabled] {
        opacity: 0.6;
        cursor: not-allowed;
    }
`;
