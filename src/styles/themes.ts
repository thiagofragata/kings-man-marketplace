const themes = {
  colors: {
    dark: "#121212",
    gray: "#808080",
    light: "#FFFFFF",
  },
};

export default themes;
