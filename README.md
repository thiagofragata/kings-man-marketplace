
# King's Man

Um marketplace de relogios de pulso, criado a partir de um projeto react com CRA.


## Installation

Install my-project with npm

```bash
    git clone https://thiagofragata@bitbucket.org/thiagofragata/kings-man-marketplace.git

    cd kings-man-marketplace

    yarn 

    yarn start
```


## Layout

https://www.figma.com/file/9eiqg4mchU0vLbcz6Gi6ac/Kingsman?node-id=0%3A1

## Demo

https://kings-man-marketplace.vercel.app/
  
## Authors

- [@thiagofragata](https://www.github.com/thiagofragata)

  